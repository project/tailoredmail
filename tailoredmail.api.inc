<?php
// $Id: tailoredmail.api.inc,v 1.1 2009/10/28 18:59:01 davyvandenbremt Exp $

/**
 * @file
 * TailoredMail API call wrappers
 *
 * @author John Locke, based on code by
 * @author Davy Van Den Bremt
 */

@require_once(drupal_get_path('module', 'tailoredmail') .'/TMBase.class.php');

/**
 * Subscribe a user to a list.
 *
 * @param $email
 *   String; E-mail address to subscribe
 * @param $fields
 *   Array; Array of custom field values. Key is field. Value is value for the field.
 * @param $lid
 *   String; List ID of the list to subscribe to.
 * @return
 *   Boolean; TRUE if user is subscribed. FALSE if not.
 */
function tailoredmail_api_subscribe($email, $fields, $lid) {
  // do api call
  $result = _tailoredmail_api_call('PartnerSubscriber', array($email, $fields, $lid));
  if (!$result) return FALSE;

  return TRUE;
}

/**
 * Unsubscribe a user from a list.
 *
 * @param $email
 *   String; E-mail address to subscribe
 * @param $lid
 *   String; List ID of the list to subscribe to.
 * @return
 *   Boolean; TRUE if user is subscribed. FALSE if not.
 */
function tailoredmail_api_unsubscribe($email, $lid) {
  // do api call
  $result = _tailoredmail_api_call('subscriberUnsubscribe', array($email, $lid));

  if (!$result) return FALSE;

  return TRUE;
}

/**
 * Fetch subscribed subscribers from API.
 *
 * @param $date
 *   Mixed; If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise, a Unix timestamp.
 * @param $lid
 *   String; List ID
 * @return
 *   Array; List of subscriber lists.
 */
function tailoredmail_api_get_subscribers_subscribed($date = 0, $lid = NULL) {
  $result = _tailoredmail_api_get_subscribers_active($date, $lid);
 // dpm($result);
  
  return $result;
}

/**
 * Fetch unsubscribed subscribers from API.
 *
 * @param $date
 *   Mixed; If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise, a Unix timestamp.
 * @param $lid
 *   String; List ID
 * @return
 *   Array; List of subscriber lists.
 */
function tailoredmail_api_get_subscribers_unsubscribed($date = 0, $lid = NULL) {
  return _tailoredmail_api_get_subscribers_active($date, $lid, 'GetUnsubscribed');
}

/**
 * Fetch subscribers from API.
 *
 * @param $date
 *   Mixed; If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise, a Unix timestamp.
 * @param $lid
 *   String; List ID
 * @param $action
 *   String; Set the actual API method to call. Defaults to Subscribers.GeActive if no other valid value is given.
 * @return
 *   Array; List of subscriber lists.
 */
function _tailoredmail_api_get_subscribers_active($date = 0, $lid = NULL, $action = 'GetSubscribed') {
  return _tailoredmail_api_get_scalar_array_call('subscribersGetActive', array($date, $lid, $action), 'Subscriber', 'EmailId');
}

/**
 * Fetch lists from API.
 *
 * @return
 *   Array; List of subscriber lists.
 */
function tailoredmail_api_get_lists() {
  $fields = array(
    'lid' => 'customListId',
    'name_api' => 'customListTitle',
  );

  return _tailoredmail_api_get_array_call('getCustomListsByDatabaseId', array(), 'List', $fields, 'lid');
}

/**
 * Fetch custom fields for some list from API.
 *
 * @param $lid
 *   String; List ID of the list.
 * @return
 *   Array; List of custom fields.
 */
function tailoredmail_api_get_custom_fields($lid) {
  $fields = array(
    'key' => 'key',
    'name' => 'name',
    'type' => 'type',
    'options' => 'options',
  );

  // do call
  $result = _tailoredmail_api_get_array_call('listGetCustomFields', array($lid), 'ListCustomField', $fields, 'key');
  unset($result['email']);
  return $result;
}


/**
 * Do API call.
 *
 * @param $method
 *   String; The API method to call.
 * @param $params
 *   Array; Parameters for the API call.
 * @return
 *   Array; API result array.
 */
function _tailoredmail_api_call($method, $params = array()) {
   // fetching keys and host id
  $access_key = variable_get('tailoredmail_access_key', '');
  $host_id = variable_get('tailoredmail_host_id', '');
  $url = variable_get('tailoredmail_base_url','');

  if (empty($host_id)) return FALSE;

  // do api call
  $tm = new TailoredMail($access_key, $host_id, $url);
  $result = call_user_func_array(array($tm, $method), $params);

  // if api result code is not 'ok', return false and write log
  if ($tm->errorCode) {
    watchdog('tailoredmail', 'Code - !code, Message - !message', array('!code'=>$tm->errorCode,'!message'=>$tm->errorMessage), WATCHDOG_ERROR);
    return FALSE;
  }

  return $result;
}

/**
 * Do API call and parse the $results as an array.
 *
 * @param $method
 *   String; The API method to call.
 * @param $params
 *   Array; Parameters for the API call.
 * @param $type
 *   Array; API type used as index under $result['anyType'].
 * @param $fields
 *   Array; Indexed array for field mapping. Keys are local fields. Values are API fields.
 * @param $key
 *   String; Local field which value will be used to index the result array.
 * @return
 *   Array; Result array.
 */
function _tailoredmail_api_get_array_call($method, $params, $type, $fields, $key = NULL) {
  $items = array();

  // do api call
  $results = _tailoredmail_api_call($method, $params);

  if ($results === FALSE) return FALSE;
  
  if (count($results->items)) {
    foreach ($results->items as $k=>$result) {
      $object = new stdClass();
      foreach ($fields as $local_field => $api_field) {
        $object->{$local_field} = $result[$api_field];
      }
      if ($key) {
        $items[$object->{$key}] = $object;
      } else {
        $items[$k] = $object;
      }
    }
  }
  
  return $items;
}

/**
 * Do API call and parse the $results as an array.
 *
 * @param $method
 *   String; The API method to call.
 * @param $params
 *   Array; Parameters for the API call.
 * @param $type
 *   Array; API type used as index under $result['anyType'].
 * @param $field
 *   String; Field.
 * @return
 *   Array; Result array.
 */
function _tailoredmail_api_get_scalar_array_call($method, $params, $type, $field) {
  $items = array();

  // do api call
  $result = _tailoredmail_api_call($method, $params);
  if ($result === FALSE) return FALSE;
  
  if (is_array($result)) {
    $data = $result;
  } else {
    $data = $result->getData();
  }
//  dpm($data);
  if (count($data['items'])) {
    foreach($data['items'] as $item) {
      $items[] = $item[$field];
    }
  }
  return $items;

}

/**
 * Convert a UNIX timestamp to a date TailoredMail wants to receive.
 *
 * @param $timestamp
 *   Integer; The UNIX timestamp to convert.
 * @retun
 *   String; The Date in TailoredMail format.
 */
function tailoredmail_api_unix_to_service_time($timestamp = 0) {
  if ($timestamp) {
    return date('Y-m-d H:i:s', $timestamp);
  }
  return 0;
}