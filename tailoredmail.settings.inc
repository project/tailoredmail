<?php

/**
 * TailoredMail settings form
 */
function tailoredmail_settings() {
  $form = array();

  // info
  $form['access_keys'] = array(
    '#type' => 'fieldset',
    '#title' => 'TailoredMail Access keys',
  );
  $form['access_keys']['tailoredmail_access_key_user_create'] = array(
    '#type' => 'textfield',
    '#title' => t('User Create Access Key'),
    // TODO: get correct URL for API usage documentation
    '#description' => t('Your TailoredMail Access Key for "User Create." See TailoredMail\'s <a href="http://www.tailoredmail.com/api/required/">documentation</a> for more info.'),
    '#default_value' => variable_get('tailoredmail_access_key_user_create', ''),
    '#required' => TRUE,
  );
  $form['access_keys']['tailoredmail_access_key_get_lists'] = array(
    '#type' => 'textfield',
    '#title' => t('Get Lists Access Key'),
    // TODO: get correct URL for API usage documentation
    '#description' => t('Your TailoredMail Access Key for "Get Custom Lists." See TailoredMail\'s <a href="http://www.tailoredmail.com/api/required/">documentation</a> for more info.'),
    '#default_value' => variable_get('tailoredmail_access_key_get_lists', ''),
    '#required' => TRUE,
  );
  $form['access_keys']['tailoredmail_access_key_get_subscriber'] = array(
    '#type' => 'textfield',
    '#title' => t('Get Subscriber by Email Access Key'),
    // TODO: get correct URL for API usage documentation
    '#description' => t('Your TailoredMail Access Key for "Get Subscriber by Email." See TailoredMail\'s <a href="http://www.tailoredmail.com/api/required/">documentation</a> for more info.'),
    '#default_value' => variable_get('tailoredmail_access_key_get_subscriber', ''),
    '#required' => TRUE,
  );
  $form['access_keys']['tailoredmail_access_key_get_list_subscribers'] = array(
    '#type' => 'textfield',
    '#title' => t('Get List Subscribers Access Key'),
    // TODO: get correct URL for API usage documentation
    '#description' => t('Your TailoredMail Access Key for "Get List Subscribers." See TailoredMail\'s <a href="http://www.tailoredmail.com/api/required/">documentation</a> for more info.'),
    '#default_value' => variable_get('tailoredmail_access_key_get_list_subscribers', ''),
    '#required' => TRUE,
  );
  $form['tailoredmail_host_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Host ID'),
    // TODO: get correct URL for API usage documentation
    '#description' => t('Your TailoredMail Host ID. See TailoredMail\'s <a href="http://www.tailoredmail.com/api/required/">documentation</a> for more info.'),
    '#default_value' => variable_get('tailoredmail_host_id', ''),
    '#required' => TRUE,
  );
  $form['tailoredmail_database_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Id'),
    '#description' => t('Your TailoredMail Database ID. See TailoredMail\'s <a href="http://www.tailoredmail.com/api/required/">documentation</a> for more info.'),
    '#default_value' => variable_get('tailoredmail_database_id', ''),
    '#required' => TRUE,
  );
  $form['tailoredmail_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL'),
    '#description' => t('URL for posting to TailoredMail. Default is test domain.'),
    '#default_value' => variable_get('tailoredmail_base_url', 'http://test.tm00.com/TMPartnerService/'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}