<?php

define('TAILOREDMAIL_TEST_BASE_URL','http://test.tm00.com/TMPartnerService/');

class TailoredMail {
  var $version = '1.0';
  var $errorMessage;
  var $errorCode;
  
  /**
   * Cache the information on the API location on the server
   */
  var $apiUrl;

  /**
   * Access key
   */
  var $accessKey;
  
  var $hostId;
  
  /**
   * Constructor
   *
   * @param string $accessKey
   * @param string $hostId
   * @return void
   */
  function __construct($accessKey, $hostId, $apiUrl = null) {
    $this->accessKey = $accessKey;
    $this->hostId = $hostId;
    $this->databaseId = variable_get('tailoredmail_database_id', 2603);
    
    if (empty($apiUrl)) {
      $this->apiUrl = TAILOREDMAIL_TEST_BASE_URL;
    } else {
      $this->apiUrl = $apiUrl;
    }
  }
  
  /**
   * Subscriber function
   *
   * @param string $email - Email address to subscribe
   * @param array $fields - Fields to associate with subscriber
   * @param mixed $lid - List id or array of list ids to subscribe
   * @param bool $unsubbed - Set to 1 to unsubscribe a user 
   *
   * @return bool false if operation fails, true if succeeds.
   */
  function partnerSubscriber($email, $fields, $lid, $unsubbed = 0) {
    $path = 'SubscriberService.svc/AddSubscriber';
    $this->accessKey = variable_get('tailoredmail_access_key_user_create',$this->accessKey);
    
    $obj = new TMObject('PartnerSubscriber', 'http://temp.org/PartnerSubscriber');
    $obj->email = $email;
    $obj->firstName = $fields['firstName'];
    $obj->lastName = $fields['lastName'];
    $obj->companyName = $fields['companyName'];
    $obj->title = $fields['title'];
    $obj->address = $fields['address'];
    $obj->city = $fields['city'];
    $obj->state = $fields['state'];
    $obj->zip = $fields['zip'];
    $obj->country = $fields['country'];
    $obj->phone = $fields['phone'];
    $obj->fax = $fields['fax'];
    $obj->age = $fields['age'];
    $obj->gender = $fields['gender'];
    $obj->password = $fields['password'];
    $obj->prop1 = $fields['prop1'];
    $obj->prop2 = $fields['prop2'];
    $obj->prop3 = $fields['prop3'];
    $obj->prop4 = $fields['prop4'];
    $obj->prop5 = $fields['prop5'];
    $obj->prop6 = $fields['prop6'];
    $obj->prop7 = $fields['prop7'];
    $obj->prop8 = $fields['prop8'];
    
    $obj->userid = 0;
    $obj->hostId = $this->hostId;
    $obj->refId = 12345689;
    $obj->source = 'Drupal';
    
    $obj->databaseId = $this->databaseId;
    $obj->unsubbed = $unsubbed;
    $obj->authorizationKey = $this->accessKey;
    $obj->authorizationUri = $this->apiUrl . $path;
    
    $obj->addToCustomListIds = $lid;
    $obj->removeFromCustomListIds = '';
    $obj->existsInCustomLists = '';
    $req = new TMObject('requestStatus', array('a'=>'http://temp.org/RequestStatus'));
    $req->requestStatus = ' ';
    $req->requestStatusDescription = ' ';
    $obj->requestStatus = $req;
    
    $response = $this->callServer($path, 'POST', $obj);
    if (is_a($response,'TMObject')) {
      $data = $response->getData();
    }
    return $response;

  }
  
  /**
   * TODO: verify that this does not clobber existing data attached to users
   */
  function subscriberUnsubscribe($email, $lid) {
    return $this->partnerSubscriber($email, array(), $lid, 1);
  }
  
  var $sublists = array();
  /**
   * This API call returns all known emails for a list, both subscribed and
   * unsubscribed. Skip the double-call by caching this list, and then return
   * results based on action...
   */
  function subscribersGetActive($date, $lid, $action = '') {
    $path = 'CustomListSubscriberService.svc/partnercustomlistsubscriber/' . $lid;
    $this->accessKey = variable_get('tailoredmail_access_key_get_list_subscribers',$this->accessKey);
    if (isset($this->sublists[$lid])) {
      $result = $this->sublists[$lid];
    } else {
      $result = $this->callServer($path);
      $this->sublists[$lid] = $result;
    }
    switch ($action) {
      case 'GetSubscribed':
        $status = "0";
        break;
      case 'GetUnsubscribed':
        $status = "1";
        break;
    }
    $items = array();
    foreach ($result->items as $item) {
      if (isset($status)) {
        if ($item['status'] === $status) {
          $items[] = $item;
        }
      } else {
          $items[] = $item;
      }
    }
    
    return array('items'=>$items);
  }
  
  function listGetCustomFields() {
    $fields = array(
      'mail' => array(
        'key' => 'email',
        'name' => 'Email',
        'type' => 'text',
        'options' => array(),
      ),
      'firstName' => array(
        'key' => 'firstName',
        'name' => 'First Name',
        'type' => 'text',
        'options' => array(),
        
      ),
      'lastName' => array(
        'key' => 'lastName',
        'name' => 'Last Name',
        'type' => 'text',
        'options' => array(),
        
      ),
      'companyName' => array(
        'key' => 'companyName',
        'name' => 'Company Name',
        'type' => 'text',
        'options' => array(),
        
      ),
      'title' => array(
        'key' => 'title',
        'name' => 'Title',
        'type' => 'text',
        'options' => array(),
        
      ),
      'address' => array(
        'key' => 'address',
        'name' => 'Address',
        'type' => 'text',
        'options' => array(),
        
      ),
      'city' => array(
        'key' => 'city',
        'name' => 'City',
        'type' => 'text',
        'options' => array(),
      ),
      'state' => array(
        'key' => 'state',
        'name' => 'State',
        'type' => 'text',
        'options' => array(),
        
      ),
      'zip' => array(
        'key' => 'zip',
        'name' => 'Zip Code',
        'type' => 'text',
        'options' => array(),
        
      ),
      'country' => array(
        'key' => 'country',
        'name' => 'Country',
        'type' => 'text',
        'options' => array(),
        
      ),
      'phone' => array(
        'key' => 'phone',
        'name' => 'Phone',
        'type' => 'text',
        'options' => array(),
        
      ),
      'fax' => array(
        'key' => 'fax',
        'name' => 'Fax',
        'type' => 'text',
        'options' => array(),
        
      ),
      'age' => array(
        'key' => 'age',
        'name' => 'Age',
        'type' => 'text',
        'options' => array(),
        
      ),
      'gender' => array(
        'key' => 'gender',
        'name' => 'Gender',
        'type' => 'text',
        'options' => array(),
        
      ),
      'nickName' => array(
        'key' => 'nickName',
        'name' => 'Nick Name',
        'type' => 'text',
        'options' => array(),
        
      ),
      'password' => array(
        'key' => 'password',
        'name' => 'Password',
        'type' => 'text',
        'options' => array(),
        
      ),
      'prop1' => array(
        'key' => 'prop1',
        'name' => 'prop1',
        'type' => 'text',
        'options' => array(),
      ),
      'prop2' => array(
        'key' => 'prop2',
        'name' => 'prop2',
        'type' => 'text',
        'options' => array(),
      ),
      'prop3' => array(
        'key' => 'prop3',
        'name' => 'prop3',
        'type' => 'text',
        'options' => array(),
      ),
      'prop4' => array(
        'key' => 'prop4',
        'name' => 'prop4',
        'type' => 'text',
        'options' => array(),
      ),
      'prop5' => array(
        'key' => 'prop5',
        'name' => 'prop5',
        'type' => 'text',
        'options' => array(),
      ),
      'prop6' => array(
        'key' => 'prop6',
        'name' => 'prop6',
        'type' => 'text',
        'options' => array(),
      ),
      'prop7' => array(
        'key' => 'prop7',
        'name' => 'prop7',
        'type' => 'text',
        'options' => array(),
      ),
      'prop8' => array(
        'key' => 'prop8',
        'name' => 'prop8',
        'type' => 'text',
        'options' => array(),
      ),
    );
    $response = new stdClass();
    $response->items = $fields;
    return $response;
    
  }
  
  /**
   * getCustomListsByDatabaseId
   *
   * @param integer $newsid - the account id containing the lists
   *
   * @return mixed response containing lists in account, or false on failure.
   */
  function getCustomListsByDatabaseId($DatabaseId = null) {
    $path = 'CustomListService.svc/GetCustomListsByDatabaseId';
    $this->accessKey = variable_get('tailoredmail_access_key_get_lists',$this->accessKey);
    
    $obj = new TMObject('PartnerAuthorization', 'http://temp.org/PartnerAuthorization');
    
    $obj->databaseId = empty($DatabaseId) ? $this->databaseId : $DatabaseId;
    $obj->authorizationKey = $this->accessKey;
    $obj->authorizationUri = $this->apiUrl . $path;
    
    $response = $this->callServer($path, 'POST', $obj);
    if (is_a($response,'TMObject')) {
      $data = $response->getData();
      //dpm($data);
    }
    return $response;
  
  }
  
  /**
   * CallServer
   */
  function callServer($path, $method = 'GET', $body = NULL) {
    // Authentication header and content-type must be set with all requests
    $headers = array(
      'Authorization'=> $this->accessKey,
      'Content-Type' => 'application/atom+xml',
    );
    if ($body) {
      $content = is_string($body) ? $body : $body->getXml();
    } else {
      $content = '';
    }
    $response = drupal_http_request($this->apiUrl . $path, $headers, $method, $content);
    $this->response = $response;
    if ($response->status_message != 'OK') {
      $this->errorCode = $response->code;
      $this->errorMessage = $response->error;
      return false;
    }
    $obj = new TMObject();
    $obj->loadXml($response->data);
    // check for unauthorized
    if ($obj->items[0] && $obj->items[0]['string'] && strpos($obj->items[0]['string'], 'You are not Authorized') === 0) {
      $this->errorCode = 403;
      $this->errorMessage = $obj->items[0]['string'];
      return false;
    }
    return $obj;
  }
  
}

/**
 * Represents a database object that can be serialized out as an XML document for the TailoredMail API, or
 * a standard object.
 *
 * This is on the simple side because at this point the TailoredMail API does not use attributes --
 * the only attribute set in the sample documents are namespace declarations.
 */
class TMObject {
  /**
   * Object properties
   */
  protected $_properties = array();
  
  var $rootElement;
  var $ns;
  
  /**
   * @param string $rootElement - string name of root element, if creating data structure in drupal
   * @param mixed $ns - namespace element or array or namespace attributes
   */
  function __construct($rootElement = '', $ns = null) {
    if (!empty($rootElement)){
      $this->rootElement = $rootElement;
      $this->ns = $ns;
    }
  }
  
  /**
   * loadXml
   *
   * @param string $xml - Xml body to load
   *
   * Parses Xml, and sets properties on this object
   */
  function loadXml($xmlstring) {
    $xml = new XMLReader();
    $xml->xml($xmlstring);
    $item = $items = array();
    $elcount = 0; // count of elements we've parsed
    $itemcount = 0;
    while ($xml->read()) {
      switch ($xml->nodeType) {
        case XMLReader::ELEMENT:
          if ($elcount){
            $value = trim($xml->value);
            $name = $xml->name;
            if (!empty($value)) {
              $item[$name] = $value;
              $elcount--; // no end tag
            }
          } else {
            // first element
            $this->rootElement = $xml->name;
            while ($xml->moveToNextAttribute()) {
              if (empty($this->ns)) {
                $this->ns = array();
              }
              if ($xml->name == 'xmlns') {
                $this->ns[] = $xml->value;
              } elseif (preg_match('/^xmlns:(.*)$/',$xml->name, $match)) {
                $this->ns[$match[1]] = $xml->value;
              }
            }
            
          }
          if (!$xml->isEmptyElement) {
            $elcount++;
          }
          break;
        case XMLReader::TEXT:
        case XMLReader::CDATA:
          $item[$name] = $xml->value;
          break;
        case XMLReader::END_ELEMENT:
          $name = $xml->name;
          $elcount--;
          if ($elcount == 1) { // we want first children of the root element
            $items[] = $item;
            $item = array();
          }
      }
    }
    if (count($items)){
      $this->items = $items;
    }
  }
  
  /**
   * @return string xml body to send in request
   */
  function getXml($fragment = false, $pfx = '') {
    $xml = new XMLWriter();
    $xml->openMemory();
    if (!$fragment) $xml->startDocument(); // allow XML fragments
    $pfx = empty($pfx) ? '' : $pfx.':';
      $xml->startElement($this->rootElement);
        if (is_array($this->ns)) {
          foreach ($this->ns as $prefix => $ns) {
            if (is_int($prefix)){
              $xml->writeAttribute('xmlns', $ns);
            } else {
              $xml->writeAttribute('xmlns:'.$prefix, $ns);
            }
          }
        } else {
          $xml->writeAttribute('xmlns', $this->ns);
        }
        foreach ($this->_properties as $element=> $value) {
          if (is_object($value)) {
            // fragment xml?
            if (is_a($value, 'TMObject')) {
              // get namespace prefix, use first one as prefix
              list($prfx, $val) = each($value->ns);
              $value = $value->getXml(true, $prfx);
              $xml->writeRaw($value);
            }
          } else {
            $xml->writeElement($pfx.$element, $value);
          }
        }
      $xml->endElement(); // root element
    if (!$fragment) $xml->endDocument();
    return $xml->outputMemory();
  }
  
  /**
   * @return array of properties which have been set
   */
  function properties() {
    return array_keys($this->_properties);
  }
  
  function getData() {
    if (empty($this->_properties)) {
      return $this->items;
    }
    return $this->_properties;
  }
  
  /**
   * PHP 5 magic function to get properties from object
   */
  function __get($key) {
    if (isset($this->_properties[$key])) {
      return $this->_properties[$key];
    }
  }
  
  /**
   * PHP 5 magic function to set properties on object
   */
  function __set($key, $value) {
    $this->_properties[$key] = $value;
  }
  
}